# Muistiinpanot

## Yleistä
* Normaalisti backslash escapena

* Multilinet:
    * rivi poikki normaalisti
    * seuraavan rivin alkuun (sama sisennys kuin ylemmässä) ... ja tab

* Kaikki utf-kasina

* Kirjastokeywordit yliajautuvat keissien keywordeilla

* Resurssifilujen keywordit yliajautuvat keissien keywordeilla

* Kirjastoille voi antaa aliaksia

* Kirjastojen hakujärjestyksen voi asettaa käsin in case weird shit

* Run Keywordilla voi ajaa keywordeja, esim. templatelle argumenttina keyword ja siellä Run Keywordilla ajoon

* Keywordin nimeen voi embeddata argumentteja, as in 'User "${user}" logs in' ja kutsuttuna 'User derpulies logs in'

* Alaviivalla alkavat tiedostot skipataan kokonaan

* __init__.robot on paikka globaaleille settingseille, suite setupille jne

* --include Tag ajaa vain tietyn tagin testit
