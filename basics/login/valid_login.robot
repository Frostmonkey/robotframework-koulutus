*** Settings ***
Documentation       Tests valid login
Resource            ./resources/login_resources.robot
Suite Teardown      Generic Suite Teardown


*** Test Cases ***
Test valid login
    Given User has navigated to the login page
    When User enters correct credentials
    And User submits credentials
    Then Welcome page is displayed
