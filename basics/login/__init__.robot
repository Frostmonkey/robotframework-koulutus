*** Settings ***
Documentation       This is a directory level init file.
...                 Only used when running a full folder of tests,
...                 won't be used at all when running single tests.

Suite Setup         Log     Running directory level setup
Suite Teardown      Log     Running directory level teardown
