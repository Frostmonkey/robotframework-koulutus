*** Settings ***
Documentation       Tests logout
Resource            ./resources/login_resources.robot
Suite Teardown      Generic Suite Teardown


*** Test Cases ***
Test logout
    Given User has logged in
    When User logs out
    Then Login page is displayed
