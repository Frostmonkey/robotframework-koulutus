*** Settings ***
Documentation       Tests multiple invalid logins.
...                 Includes a few generic SQL injections for fun and profit.

Resource            ./resources/login_resources.robot
Suite Setup         User has navigated to the login page
Test Setup          Go To   ${LOGIN_URL}
Test Template       Invalid Login
Suite Teardown      Close Browser

*** Test Cases ***  USERNAME                PASSWORD
Invalid Username    invalid                 ${CORRECT_PASSWORD}
Invalid Password    ${CORRECT_USERNAME}     invalid
Empty Username      ${EMPTY}                ${CORRECT_PASSWORD}
Empty Password      ${CORRECT_USERNAME}     ${EMPTY}
Empty Both          ${EMPTY}                ${EMPTY}
SQL OR Injection    admin                   admin' OR 'a'='a'
SQL DROP Injection  admin                   admin'); DROP TABLE Users; --

*** Keywords ***
Invalid Login
  [Arguments]   ${username}   ${password}
  Given Login page is displayed
  And User enters credentials    ${username}   ${password}
  When User submits credentials
  Then Error page is displayed
