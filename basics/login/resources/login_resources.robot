*** Settings ***
Documentation       Resources for login testing
Library             Selenium2Library

*** Variables ***
${LOGIN_URL}                http://localhost:7272
${CORRECT_USERNAME}         demo
${CORRECT_PASSWORD}         mode
${INCORRECT_USERNAME}       asdf
${INCORRECT_PASSWORD}       fdsa
${WELCOME_PAGE_TITLE}       Welcome Page
${ERROR_PAGE_TITLE}         Error Page
${LOGIN_PAGE_TITLE}         Login Page


*** Keywords ***
Generic Suite Teardown
    Close All Browsers

User has navigated to the login page
    Open Browser   ${LOGIN_URL}
    Login page is displayed

User enters correct credentials
    Input Text  id=username_field   ${CORRECT_USERNAME}
    Input Text  id=password_field   ${CORRECT_PASSWORD}

User enters credentials
    [Arguments]     ${username}     ${password}
    Input Text  id=username_field   ${username}
    Input Text  id=password_field   ${password}

User has logged in
    User has navigated to the login page
    User enters correct credentials
    User submits credentials

User logs out
    Click Link   xpath=//*[@id="container"]/p/a

User submits credentials
    Click Element   id=login_button

Welcome page is displayed
    Title Should Be     ${WELCOME_PAGE_TITLE}

Error page is displayed
    Title Should Be     ${ERROR_PAGE_TITLE}

Login page is displayed
    Title Should Be     ${LOGIN_PAGE_TITLE}
