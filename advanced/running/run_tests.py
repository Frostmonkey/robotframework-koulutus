#!/usr/bin/env python

import sys
from robot import run_cli

try:
    run_cli(['--name', 'Example', '--dotted'] + sys.argv[1:])
except SystemExit:
    print 'The end'
    raise
