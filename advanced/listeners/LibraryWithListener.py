class LibraryWithListener(object):
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    ROBOT_LISTENER_API_VERSION = 2

    def __init__(self):
        self.ROBOT_LIBRARY_LISTENER = self
        self.keyword_calls = {}

    # Normal keyword
    def log_keyword_calls(self):
        for kw in sorted(self.keyword_calls):
            print '{}: {}'.format(kw, self.keyword_calls[kw])

    # Listener method. Can start with '_' in this context
    # to avoid creating keyword.
    def _start_keyword(self, name, attrs):
        if name not in self.keyword_calls:
            self.keyword_calls[name] = 0
        self.keyword_calls[name] += 1
