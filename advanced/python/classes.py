class Simple(object):
    attribute = "Hello, world!"

class Person(object):
    def __init__(self, name=None, email=None):
        self.name = name
        self.email = email

    def greet(self, target, greeting="hello"):
        return "{} says {} to {}".format(self.name, greeting, target)

    def __str__(self):
        if self.email != None:
            return "{} <{}>".format(self.name, self.email)
        else:
            return self.name

    def __eq__(self, other):
        if not isinstance(other, Person):
            return False
        return self.__dict__ == other.__dict__

def get_person(name, email=None):
    return Person(name, email)

def greet_person(person, name):
    person.greet(name)
