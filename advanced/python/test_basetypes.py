from basetypes import (
    string,
    multiline_string,
    unicode_string,
    number, integer,
    boolean
)


def test_string():
    assert string == 'Hello, world!'
    assert string == "Hello, world!"


def test_multiline_string():
    assert multiline_string == 'first line\nsecond line\nlast line\n'
    assert multiline_string == '''first line
second line
last line
'''


def test_unicode_string():
    assert unicode_string == u'Hyv\xe4'


def test_string_methods():
    assert string.startswith('Hello')
    assert 'world' in string
    assert string.lower() == 'hello, world!'
    assert unicode_string.upper() == u'HYV\xc4'
    assert string + '!!?!?!' == 'Hello, world!!!?!?!'
    assert string * 2 == 'Hello, world!Hello, world!'


def test_numbers():
    assert number == 3.14
    assert integer == 42
    assert integer * 2 == 84
    assert number + integer == 45.14


def test_boolean():
    assert boolean is True
    assert boolean == True    # Not recommended
    assert boolean is not False
