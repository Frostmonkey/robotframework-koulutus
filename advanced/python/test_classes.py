from classes import (
    Simple,
    Person
)


def test_instantiation():
    s = Simple()
    t = Simple()
    assert isinstance(s, Simple)
    assert isinstance(t, Simple)
    assert s is not t
    assert s != t


def test_class_and_instance_attributes():
    assert Simple.attribute == 'Hello, world!'
    instance = Simple()
    assert instance.attribute == 'Hello, world!'
    instance.attribute = 42
    assert instance.attribute == 42
    assert Simple.attribute == 'Hello, world!'


def test_instantiation_with_arguments():
    jane = Person('Jane')
    assert jane.name == 'Jane'
    assert jane.email is None
    assert not hasattr(Person, 'name')
    john = Person('John', 'john@example.com')
    assert john.name == 'John'
    assert john.email == 'john@example.com'


def test_methods():
    jane = Person('Jane')
    assert jane.greet('John') == 'Jane says hello to John'
    assert Person('John').greet('Jane', greeting='hi') == 'John says hi to Jane'


def test_string_representation():
    jane = Person('Jane')
    assert str(jane) == 'Jane'
    assert jane != 'Jane'
    jane = Person('Jane', 'j@a.ne')
    assert str(jane) == 'Jane <j@a.ne>'
    assert 'To: %s' % jane == 'To: Jane <j@a.ne>'


def test_equality():
    assert Person('Jane') != Person('John')
    assert Person('Jane') == Person('Jane')
    assert Person('Jane') != Person('Jane', 'e@mail')
    assert Person('Jane', 'e@mail') == Person('Jane', 'e@mail')
    assert not (Person('Jane') == 42)
