from control import (
    is_positive,
    select_positive
)


def test_if_else():
    assert is_positive(1) == '1 is positive'
    assert is_positive(-1.5) == '-1.5 is negative'
    assert is_positive(0) == '0 is zero'


def test_truth():
    assert all([True, 'string', 1, ['list'], ('tuple',), {'di': 'ct'}, {'set'}])
    assert not any([False, None, '', 0, [], (), {}, set()])


def test_looping():
    assert select_positive([1, -2, 0, -1.5, 7, 1.1]) == [1, 7, 1.1]


def test_exceptions():
    try:
        is_positive('xxx')
    except TypeError as err:
        assert str(err) == "'xxx' is not a number"
    else:
        raise AssertionError('Expected TypeError')
