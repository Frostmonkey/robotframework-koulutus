from pprint import pformat

def simple():
    return "Hello, world!"

def one_argument(target):
    return "Hello, {}!".format(target)

def two_arguments(first, second):
    return float(first) + float(second)

def defaults(target, ending="!", separator=", "):
    return "Hello{}{}{}".format(separator, target, ending)

def varargs(*numbers):
    return sum(int(n) for n in numbers)

def kwargs(**kws):
    return ', '.join('{}: {}'.format(k, kws[k]) for k in sorted(kws))

def caller(funcname, *args, **kwargs):
    return funcname(*args, **kwargs)
