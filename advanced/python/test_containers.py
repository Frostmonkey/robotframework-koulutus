from containers import (
    alist,
    atuple,
    adict,
    aset
)


def test_list():
    assert alist == ['Hello', 'world']
    assert alist[0] == 'Hello'
    assert alist[-1] == 'world'
    assert alist[:1] == ['Hello']
    assert alist[1:] == ['world']
    assert 'Hello' in alist


def test_list_is_mutable():
    blist = alist
    clist = alist[:]
    assert alist == blist == clist
    assert alist is blist
    assert alist is not clist
    alist.append('!!')
    blist[0] = 'Hillo'
    assert alist == ['Hillo', 'world', '!!']
    assert alist == blist
    assert alist != clist
    assert clist == ['Hello', 'world']
    blist = 'Not even a list'
    assert alist != blist


def test_globality():
    assert alist == ['Hillo', 'world', '!!']
    try:
        blist
    except NameError:
        pass


def test_tuple():
    assert atuple == ('Hello', 'world')
    assert atuple != ['Hello', 'world']
    assert list(atuple) == ['Hello', 'world']
    assert atuple[0] == 'Hello'
    assert atuple[-1] == 'world'
    assert atuple[:1] == ('Hello',)  # trailing comma needed with one item tuple
    assert atuple[1:] == ('world',)
    assert 'Hello' in atuple


def test_tuple_is_not_mutable():
    try:
        atuple[0] = 'tuples are not mutable'
    except TypeError:
        pass


def test_dict():
    assert adict == {'a': 1, 'b': 2}
    assert adict['a'] == 1
    assert 'a' in adict
    assert 1 not in adict
    assert 1 in adict.values()


def test_iterating_dict_returns_keys():
    assert sorted(adict) == ['a', 'b']


def test_dict_is_mutable():
    bdict = adict
    cdict = adict.copy()
    assert adict == bdict == cdict
    assert adict is bdict
    assert adict is not cdict
    adict['b'] = 3
    assert adict == {'a': 1, 'b': 3}
    assert adict == bdict
    assert adict != cdict


def test_set():
    assert aset == {1, 2, 3, 4, 5}
    assert 1 in aset
    assert 0 not in aset


def test_set_is_iterable_but_not_indexable():
    assert sorted(aset) == [1, 2, 3, 4, 5]
    try:
        aset[0]
    except TypeError:
        pass


def test_set_is_mutable():
    bset = aset
    cset = aset.copy()
    assert aset == bset == cset
    aset.add(0)
    assert aset == {0, 1, 2, 3, 4, 5}
    assert aset == bset
    assert aset != cset
