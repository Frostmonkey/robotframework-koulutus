from functions import (
    simple,
    one_argument, two_arguments,
    defaults,
    varargs,
    kwargs,
    caller
)


def test_simple():
    assert simple() == 'Hello, world!'


def test_arguments():
    assert one_argument('world') == 'Hello, world!'
    assert one_argument('tellus') == 'Hello, tellus!'
    assert two_arguments(1, 2) == 3


def test_arguments_with_default_values():
    assert defaults('world') == 'Hello, world!'
    assert defaults('tellus', '!!?!?') == 'Hello, tellus!!?!?'
    assert defaults('tellus', ending='!!?!?') == 'Hello, tellus!!?!?'
    assert defaults('again', separator=' ', ending='...') == 'Hello again...'
    assert defaults('again', '...', ' ') == 'Hello again...'


def test_variable_number_of_arguments():
    assert varargs() == 0
    assert varargs(1) == 1
    assert varargs(1, 2) == 3
    assert varargs(1, 2, 3, 4, 5) == 15


def test_keyword_arguments():
    assert kwargs() == ''
    assert kwargs(a=1) == 'a: 1'
    assert kwargs(x=1, z=3, y=2) == 'x: 1, y: 2, z: 3'


def test_caller():
    assert caller(simple) == 'Hello, world!'
    assert caller(one_argument, 'called') == 'Hello, called!'
    assert caller(defaults, 'tellus', ending='!!?!?') == 'Hello, tellus!!?!?'
    assert caller(varargs, 1, 2) == 3
    assert caller(kwargs, foo=1, bar=2) == 'bar: 2, foo: 1'
