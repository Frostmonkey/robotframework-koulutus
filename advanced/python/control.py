from numbers import Number


def is_positive(number):
    try:
        if not isinstance(number, Number):
            number = float(number)
    except ValueError:
        raise TypeError("'{}' is not a number".format(number))
    if number > 0:
        template = '{} is positive'
    elif number < 0:
        template = '{} is negative'
    else:
        template = '{} is zero'
    return template.format(number)

def select_positive(values):
    return [x for x in values if x > 0]
