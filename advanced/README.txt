This directory contains Python training material.

Files starting with `test_` contain exercises. They can be executed with
`py.test` tool that can be installed with `pip install pytest` and used like:

    py.test test_basetypes.py       # run all tests in test_basetypes.py
    py.test test_basetypes.py -x    # same but stop on the first failure
    py.test .                       # run all tests in the current directory

Additional Python files need to be created to make the test pass.

There is a lot of Python related training material freely available in the
Internet. You can start from the official Python Tutorial
<https://docs.python.org/2/tutorial/> or look at what other resources are
available at https://www.python.org/about/gettingstarted/.
