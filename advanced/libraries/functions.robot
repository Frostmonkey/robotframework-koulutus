*** Settings ***
Library          ../python/functions.py
Library          ../python/control.py
Library          ../python/classes.py

*** Test Cases ***
Simple
    Simple

Arguments
    ${result} =    One Argument    world
    Should Be Equal    ${result}    Hello, world!
    ${result} =    Two Arguments    1    2
    Should Be Equal    ${result}    ${3}

Defaults
    ${result} =    Defaults    world
    Should Be Equal    ${result}    Hello, world!
    ${result} =    Defaults    world    !!!
    Should Be Equal    ${result}    Hello, world!!!
    ${result} =    Defaults    again    separator=${SPACE}    ending=...
    Should Be Equal    ${result}    Hello again...

Varargs
    ${result} =    Varargs    1    2    3    4    5
    Should Be Equal    ${result}    ${15}

Kwargs
    ${result} =    Kwargs    a=1    b=2    c=3
    Should Be Equal    ${result}    a: 1, b: 2, c: 3

Failure
    Is Positive    1
    Is Positive    foo

Object as a return value
    ${jane}=    Get Person  Jane    e@mail.com
    Log     ${jane}
    Greet Person    ${jane}     John

Extended variable syntax
    ${jane}=    Get Person  Jane    e@mail.com
    Log Many
    ...     ${jane.name}
    ...     ${jane.email}
    ...     ${jane.greet('Bob Ross')}               # please don't
    ...     ${jane.greet('${jane.email}')}          # oh dear god no.
