*** Settings ***
Library         CalculatorLibrary

*** Test Cases ***

Push Buttons (ugly)
    ${result}=          Push Button     1
    Should Be Equal     ${result}       1
    ${result}=          Push Button     2
    Should Be Equal     ${result}       12

Push Buttons
    Push Button         1
    Push Button         2
    Result Should Be    12

Calculation
    Push Button         1
    Push Button         +
    Push Button         2
    Push Button         =
    Result Should Be    3

Longer Calculation With Varargs
    Calculate Varargs        1   +   2   -  5   +   2   =
    Result Should Be    0

Longer Calculation
    Calculate           1+2-5+2=
    Result Should Be    0

    Calculate           1 + 2 - 5 + 2 =
    Result Should Be    0

Invalid Expression
    Calculation Should Fail     invalid
    Calculation Should Fail     1 / 0
    Calculation Should Fail     1 + * 2
