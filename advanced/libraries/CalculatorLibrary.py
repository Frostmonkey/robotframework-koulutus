from calculator import Calculator, CalculationError
from robot.api import logger

class CalculatorLibrary(object):

    def __init__(self):
        self.calculator = Calculator()
        self.result = ''

    def push_button(self, button):
        """ Pushes button ``button``. Yay.

        Current result is returned. Can also be verified using `Result Should Be`.

        Use `Calculate` for longer calculations.
        """
        logger.debug('Pushing button {}'.format(button))
        self.result = self.calculator.push(button)
        return self.result

    def calculate_varargs(self, *args):
        logger.debug('Calculating {}'.format(args))
        for button in args:
            self.result = self.calculator.push(button)

    def calculate(self, calculation):
        logger.debug('Calculating {}'.format(calculation))
        for button in calculation.replace(' ', ''):
            self.result = self.calculator.push(button)

    def result_should_be(self, expected):
        if self.result != expected:
            raise AssertionError('Expected result to be {} but it was {}'
                                 .format(expected, self.result))
        return True

    def calculation_should_fail(self, calculation):
        try:
            self.calculate(calculation)
        except CalculationError as err:
            logger.debug('Received error {}'.format(err))
        else:
            raise AssertionError('Expected "{}" to be invalid but it wasn\'t'.format(calculation))
