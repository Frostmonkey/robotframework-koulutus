This directory introduces variable files.

`variables.py` is a very simple variable file containing variables directly
as attributes.

`dynamic_variables.py` demonstrates having a function that returns variables.

See the Robot Framework User Guide for more information:
http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#variable-files
