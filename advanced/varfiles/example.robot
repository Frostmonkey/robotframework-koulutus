*** Settings ***
Variables        ${PATH}/variables.py
Variables        dynamic_variables.py    Jane    jane@example

*** Variables ***
${NEW STRING}    ${STRING} world
${PATH}          .

*** Test Cases ***
Simple
    Should Be Equal    ${STRING}    Hello
    Should Be Equal    ${NEW STRING}    Hello world
    Should Be Equal    ${INTEGER}   ${42}
    Should Contain    ${LIST}    xxx
    Log Many    @{LIST}
    Log    ${RANDOM INTEGER}

Dynamic Variables
    Should Be Equal    ${NAME}    Jane
    Should Be Equal    ${EMAIL}    jane@example

Extended Variable Syntax
    Should Be Equal    ${PERSON.name}    Jane
    Should Be Equal    ${PERSON.email}    jane@example
