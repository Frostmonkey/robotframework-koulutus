def get_variables(name, email=None):
    return {'name': name, 'email': email,
            'person': Person(name, email)}


class Person(object):

    def __init__(self, name, email=None):
        self.name = name
        self.email = email
