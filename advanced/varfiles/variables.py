import random as _random    # Don't expose random as variable

# Alternatively __all__ can be used to control what is exposed:
# __all__ = ['STRING', ...]

STRING = "Hello"
INTEGER = 42
LIST = ['xxx', 'yyy', 'zzz']
RANDOM_INTEGER = _random.randint(0, 100)
